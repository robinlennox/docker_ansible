
```
docker run -v ${PWD}:/opt/ansible --rm robinlennox/docker_ansible ansible-playbook --version
```
# Configuration

In the usage example it uses the present(current) working directory `-v ${PWD}:/opt/ansible` to mount to the ansible docker container. `${PWD}` can be change to any directory you like.

# Add Bash Alias
## ansible-playbook
To run ansible-playbook easier create an alias in bash:

```sh
alias ansible-playbook="sudo docker run -v ${PWD}:/opt/ansible --rm robinlennox/docker_ansible ansible-playbook"
```
# Using ansible-vault

Start a shell in the ansible docker container

```sh
sudo docker run -v ${PWD}:/opt/ansible -it robinlennox/docker_ansible:latest /bin/bash
```

Then run the command the edit the vault

```sh
ansible-vault edit ./vars/secret
```
