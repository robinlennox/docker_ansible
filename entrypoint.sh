#/bin/bash
# find the UID and GID used for the directory
dir_uid=$(ls -an | head -2  | tail -n -1 | awk '{print $3}')
dir_gui=$(ls -an | head -2  | tail -n -1 | awk '{print $4}')

#UID to Ansible user
usermod -u ${dir_uid} ansible > /dev/null 2>&1
#GID to Ansible user
groupmod -g ${dir_gui} ansible > /dev/null 2>&1

# Run as ansible user
exec runuser -u ansible "$@"
