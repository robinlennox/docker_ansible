FROM python:3.9-slim
LABEL maintainer="Robin Lennox"

RUN apt-get update && \
    apt-get install -y --no-install-recommends vim nano dnsutils git curl azure-cli && \ 
    rm -rf /var/lib/apt/lists/* && \
    pip3 install ansible==6.7.0 ansible-lint jmespath mitogen pywinrm requests yamllint linode_api4  && \
    pip3 cache purge && \
    curl -O https://raw.githubusercontent.com/ansible-collections/azure/dev/requirements-azure.txt && \
    pip3 install -r requirements-azure.txt && \
    rm requirements-azure.txt && \
    groupadd -r ansible && \
    useradd --no-log-init -m -g ansible ansible && \
    mkdir /opt/ansible && \
    runuser -l ansible -c 'ansible-galaxy collection install scicore.guacamole vultr.cloud ngine_io.vultr azure.azcollection && ansible-galaxy install git+https://github.com/wazuh/wazuh-ansible.git,v4.3.10'
COPY ansible.cfg /home/ansible/.ansible.cfg
RUN chown -R ansible:ansible /home/ansible/.ansible.cfg
WORKDIR /opt/ansible
COPY entrypoint.sh /home/ansible/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/home/ansible/entrypoint.sh"]
CMD ["ansible-playbook"]
